# Mini-chat (OpenClassroums)

[](https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/914663-tp-un-mini-chat)
https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/914663-tp-un-mini-chat

# OOP MVC Website (Eduonix)

[](https://www.eduonix.com/courses/Web-Development/learn-object-oriented-php-by-building-a-complete-website)
https://www.eduonix.com/courses/Web-Development/learn-object-oriented-php-by-building-a-complete-website


# PHP Send Mail Form

[](https://github.com/simplonco/php-send-mail-form)
https://github.com/simplonco/php-send-mail-form


# Wordpress

[](https://github.com/simplonco/wordpress-training)
https://github.com/simplonco/wordpress-training